# Description
REST service (JSON/HTTP) for storing information about gateways and their associated devices.<br>

Programming language: C#<br>
Framework: ASP.Net<br>
Database: MSSQL local file //localdb.mdf<br>
    

# Configuration
-Reconnect the local database only if necessary<br>
-Build the application<br>
-run the application<br>
    
# Endpoints

# Create a gateway
POST http://localhost:44390/gateways<br>
body: {<br>
"sn": "string", //a unique serial number ex: AbC123 // auto generated too<br>
"name": "string", //a human-readable name ex: Gateway A<br>
"ip": "string" //an IPv4 address ex: 10.0.0.1<br>
}

# Delete a gateway
DELETE http://localhost:44390/gateways/{sn}

# Get all stored gateways
GET http://localhost:44390/gateways

# Get a single gateway
GET http://localhost:44390/gateways/{sn}

# Add a device from a gateway
POST http://localhost:44390/devices

body: {<br>
"vendor": "string", // ex: Vendor A<br>
"status": "online|offline" // ex: online<br>
Gateway_sn: "string"<br>
}

# Remove a device from a gateway
DELETE http://localhost:44390/devices/{uid}