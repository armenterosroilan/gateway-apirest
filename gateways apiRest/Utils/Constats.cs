﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace gateways_apiRest.Utils
{
    public class Constants
    {
        public const string MAX_DEVICE_EXCEPTION_MESSAGE = "No more than 10 devices are allowed in a Gateway";

        public const string INVALID_IP_ADDRESS_EXCEPTION_MESSAGE = "Please provide a valid IP address";
    }
}