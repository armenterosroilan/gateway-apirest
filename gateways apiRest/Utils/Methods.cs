﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace gateways_apiRest.Utils
{
    public class Methods
    {
        public Boolean validateIp(string ip) {
            Regex ipRgx = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
            return ipRgx.IsMatch(ip);
        }
    }
}