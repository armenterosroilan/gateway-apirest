﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using gateways_apiRest.Models;
using gateways_apiRest.Utils;
using gateways_apiRest.Exceptions;

namespace gateways_apiRest.Controllers
{
    public class GatewaysController : ApiController
    {
        private ModelContainer db = new ModelContainer();

        // GET: api/Gateways
        public IQueryable<Gateway> GetGateways()
        {
            return db.Gateways;
        }

        // GET: api/Gateways/5
        [ResponseType(typeof(Gateway))]
        public async Task<IHttpActionResult> GetGateway(int id)
        {
            Gateway gateway = await db.Gateways.FindAsync(id);
            if (gateway == null)
            {
                return NotFound();
            }

            return Ok(gateway);
        }

        // PUT: api/Gateways/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGateway(int id, Gateway gateway)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gateway.sn)
            {
                return BadRequest();
            }

            db.Entry(gateway).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GatewayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Gateways
        [ResponseType(typeof(Gateway))]
        public async Task<IHttpActionResult> PostGateway(Gateway gateway)
        {
            Methods utils = new Methods();
            if (!utils.validateIp(gateway.ip)) {
                ExceptionBuilder ex = new ExceptionBuilder();

                throw new HttpResponseException(ex.buildException(Constants.INVALID_IP_ADDRESS_EXCEPTION_MESSAGE));
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Gateways.Add(gateway);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = gateway.sn }, gateway);
        }

        // DELETE: api/Gateways/5
        [ResponseType(typeof(Gateway))]
        public async Task<IHttpActionResult> DeleteGateway(int id)
        {
            Gateway gateway = await db.Gateways.FindAsync(id);
            if (gateway == null)
            {
                return NotFound();
            }

            db.Gateways.Remove(gateway);
            await db.SaveChangesAsync();

            return Ok(gateway);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GatewayExists(int id)
        {
            return db.Gateways.Count(e => e.sn == id) > 0;
        }
    }
}