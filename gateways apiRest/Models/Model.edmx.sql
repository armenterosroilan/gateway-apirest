
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/06/2020 23:01:57
-- Generated from EDMX file: C:\Users\User\documents\visual studio 2017\Projects\gateways apiRest\gateways apiRest\Models\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [localdb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GatewayDevice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Devices] DROP CONSTRAINT [FK_GatewayDevice];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Gateways]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Gateways];
GO
IF OBJECT_ID(N'[dbo].[Devices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Devices];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Gateways'
CREATE TABLE [dbo].[Gateways] (
    [sn] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [ip] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Devices'
CREATE TABLE [dbo].[Devices] (
    [uid] int IDENTITY(1,1) NOT NULL,
    [vendor] nvarchar(max)  NOT NULL,
    [status] int  NOT NULL,
    [Gateway_sn] int  NOT NULL,
    [createdDate] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [sn] in table 'Gateways'
ALTER TABLE [dbo].[Gateways]
ADD CONSTRAINT [PK_Gateways]
    PRIMARY KEY CLUSTERED ([sn] ASC);
GO

-- Creating primary key on [uid] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [PK_Devices]
    PRIMARY KEY CLUSTERED ([uid] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Gateway_sn] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [FK_GatewayDevice]
    FOREIGN KEY ([Gateway_sn])
    REFERENCES [dbo].[Gateways]
        ([sn])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GatewayDevice'
CREATE INDEX [IX_FK_GatewayDevice]
ON [dbo].[Devices]
    ([Gateway_sn]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------