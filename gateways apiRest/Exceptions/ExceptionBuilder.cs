﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace gateways_apiRest.Exceptions
{
    public class ExceptionBuilder
    {
        public HttpResponseMessage buildException(string msg)
        {
            var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(msg),
                StatusCode = HttpStatusCode.BadRequest
            };
            return response;
        }
    }
}